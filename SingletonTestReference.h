/*
 * SingletonTestReference.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef SINGLETONTESTREFERENCE_H_
#define SINGLETONTESTREFERENCE_H_

class SingletonTestReference {
public:
	virtual ~SingletonTestReference();
	static SingletonTestReference& getInstance();

private:
	SingletonTestReference();
	SingletonTestReference(const SingletonTestReference& other);
};

#endif /* SINGLETONTESTREFERENCE_H_ */
