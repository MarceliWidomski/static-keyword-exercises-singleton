//============================================================================
// Name        : static.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "CounterOfObjects.h"
#include "SingletonTest.h"
#include "SingletonTestReference.h"

int main() {

	SingletonTest* first = SingletonTest::getInstance();
	SingletonTest* second = SingletonTest::getInstance();
	std::cout << "Address of first: " << first << std::endl;
	std::cout << "Address of second: " << second << std::endl;
	first->setTestValue(10);
	std::cout << "testValue called by first: " << first->getTestValue() << std::endl;
	std::cout << "testValue called by second: " << second->getTestValue() << std::endl;
	SingletonTestReference& firstReference = SingletonTestReference::getInstance();
	SingletonTestReference& secondReference = SingletonTestReference::getInstance();
	std::cout << "Address of first reference: " << &firstReference << std::endl;
	std::cout << "Address of second reference: " << &secondReference << std::endl;

	CounterOfObjects one;
	CounterOfObjects two;
	{
		CounterOfObjects three(one);
		CounterOfObjects four = one;
	}
	std::cout << "Number of created objects: " << one.getObjectsNumber()
			<< std::endl;
	std::cout << "Number of existing objects: "
			<< one.getExistingObjectsNumber() << std::endl;
	std::cout << "Value of test called by object one: " << one.getTest()
			<< std::endl;
	std::cout << "Changing value of test by object one... " << std::endl;
	one.setTest(5.789);
	std::cout << "Value of test called by object two: " << two.getTest()
			<< std::endl;
	std::cout << "Value of test called by scope operator: "
			<< CounterOfObjects::getTest() << std::endl;

	return 0;
}
