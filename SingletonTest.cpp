/*
 * SingletonTest.cpp
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#include "SingletonTest.h"



SingletonTest::~SingletonTest() {
}
SingletonTest::SingletonTest() {
	testValue = 0;
}
SingletonTest::SingletonTest(const SingletonTest& other){
	testValue = 0;
}
SingletonTest* SingletonTest::getInstance(){
	static SingletonTest* ptrSingletonTest = new SingletonTest;
	return ptrSingletonTest;
}
