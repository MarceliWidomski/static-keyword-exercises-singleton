/*
 * SingletonTestReference.cpp
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#include "SingletonTestReference.h"

SingletonTestReference::~SingletonTestReference() {
}
SingletonTestReference::SingletonTestReference() {
}
SingletonTestReference::SingletonTestReference(const SingletonTestReference& other){
}
SingletonTestReference& SingletonTestReference::getInstance(){
	static SingletonTestReference* singletonReferenceTest = new SingletonTestReference;
	return *singletonReferenceTest;
}
