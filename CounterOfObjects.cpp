/*
 * CounterOfObjects.cpp
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#include "CounterOfObjects.h"
#include <iostream>

int CounterOfObjects::objectsNumber = 0;
int CounterOfObjects::existingObjectsNumber = 0;
double CounterOfObjects::test = 22.2;

CounterOfObjects::CounterOfObjects() {
std::cout << "Calling default constructor..." <<std::endl;
objectsNumber++;
existingObjectsNumber++;
}

CounterOfObjects::CounterOfObjects(const CounterOfObjects& other){
	std::cout << "Calling copying constructor..." << std::endl;
	this->objectsNumber++;
	this->existingObjectsNumber++;
}

CounterOfObjects::~CounterOfObjects() {
std::cout << "Calling destructor..." <<std::endl;
existingObjectsNumber--;
}

