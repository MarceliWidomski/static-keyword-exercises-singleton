/*
 * SingletonTest.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef SINGLETONTEST_H_
#define SINGLETONTEST_H_

class SingletonTest {
public:
	virtual ~SingletonTest();
	static SingletonTest* getInstance();

	int getTestValue() const {return testValue;}
	void setTestValue(int testValue) {this->testValue = testValue;}


private:
	SingletonTest();
	SingletonTest(const SingletonTest& other);
	int testValue;
};

#endif /* SINGLETONTEST_H_ */
