/*
 * CounterOfObjects.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef COUNTEROFOBJECTS_H_
#define COUNTEROFOBJECTS_H_

class CounterOfObjects {
public:
	CounterOfObjects();
	CounterOfObjects(const CounterOfObjects& other);
	virtual ~CounterOfObjects();

	static int getObjectsNumber() {return objectsNumber;}
	static void setObjectsNumber(int _objectsNumber) {objectsNumber = _objectsNumber;}

	static int getExistingObjectsNumber() {return existingObjectsNumber;}
	static void setExistingObjectsNumber(int _existingObjectsNumber) {existingObjectsNumber = _existingObjectsNumber;}

	static double getTest() {return test;}
	static void setTest(double _test) {test = _test;}


private:
	static int objectsNumber;
	static int existingObjectsNumber;
	static double test;
};

#endif /* COUNTEROFOBJECTS_H_ */
